var map;
var latlng = [];
var markers = [];
var startAllowed;
var pos = [];
var posIndex = 0;

function initialize() 
{
	rendermap();
	retrieveMarkerLocations();
}

function rendermap() 
{
	var mapProp = 
	{
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map($("#googleMap")[0], mapProp);
}

function retrieveMarkerLocations() 
{
	$(function() 
	{
		$.get("/application/geolocations", function(data) 
		{
			$.each(data, function(index, geoObj) 
			{
				console.log(geoObj[0] + " " + geoObj[1] + " " + geoObj[2]);
			});
			callback(data);
		});
	});
}

function callback(data) 
{

	latlng = data;
	fitBounds(latlng);
	setInfoWindowListener(latlng);
	populateTable();
}

function fitBounds(latlngStr) 
{
	var bounds = new google.maps.LatLngBounds();
	for (i = 0; i < latlngStr.length; i++) 
	{
		marker = new google.maps.Marker(
		{
			position : getLatLng(latlngStr[i]),
			map : map
		});
		markers[i] = marker;
		bounds.extend(marker.position);
	}
	map.fitBounds(bounds);
}

function setInfoWindowListener(latlngStr) 
{
	var infowindow = new google.maps.InfoWindow();
	for (i = 0; i < latlng.length; i++) 
	{
		var marker = markers[i];
		google.maps.event.addListener(marker, 'click', (function(marker, i) 
		{
			return function() 
			{
				infowindow.setContent(latlngStr[i][0]);
				infowindow.open(map, marker);
			}
		})(marker, i));
	}
}

function getLatLng(str) 
{

	var lat = Number(str[1]);
	var lon = Number(str[2]);
	return new google.maps.LatLng(lat, lon);
}

function start() 
{
  if (startAllowed == false) 
  {
    alert("Reset to Start");
    return;
  }
  
  $('#markertable').empty();
  listenerHandler = google.maps.event.addListener(map, 'click', function(e) 
  {
    pos[posIndex] = e.latLng;
    if (posIndex > 0) 
    {
      polyline(posIndex - 1, posIndex);
    }
    posIndex += 1;
  });

}

function stop() 
{
  polyline(pos.length - 1, 0);
  drawPolygon();
  google.maps.event.removeListener(listenerHandler);
  listenerHandler = null;
  startAllowed = false;
}

function filter() 
{
	  for (var i = 0; i < latlng.length; i += 1) 
	  {
	    var point = new google.maps.LatLng(latlng[i][1], latlng[i][2]);
	    if (google.maps.geometry.poly.containsLocation(point, polygon)) 
	    {
	      markers[i].setVisible(true);
	      populateTableRow(latlng[i]);
	    } else 
	    {
	      markers[i].setVisible(false);
	    }
	  }
}

function reset() 
{
  location.reload();
}

function polyline(prevIndex, index) 
{
  var coords = [
      new google.maps.LatLng(pos[prevIndex].lat(), pos[prevIndex].lng()),
      new google.maps.LatLng(pos[index].lat(), pos[index].lng())];

  var line = new google.maps.Polyline(
  {
    path : coords,
    geodesic : true,
    strokeColor : '#FF0000',
    strokeOpacity : 1.0,
    strokeWeight : 2
  });
  line.setMap(map);
}

function drawPolygon() 
{
  var lineCoords = [];
  for (var j = 0; j < pos.length; j += 1) 
  {
    console.log(pos[j].lat + " " + pos[j].lng);
    lineCoords[j] = new google.maps.LatLng(pos[j].lat(), pos[j].lng());
  }
  lineCoords[pos.length] = new google.maps.LatLng(pos[0].lat(), pos[0].lng());

  polygon = new google.maps.Polyline(
  {
    path : lineCoords,
    geodesic : true,
    strokeColor : '#FF0000',
    strokeOpacity : 1.0,
    strokeWeight : 2
  });

  polygon.setMap(map);
  google.maps.event.clearListeners(map, 'click');
}

function populateTable()
{
  $.each(latlng, function(i, val) 
  {
    populateTableRow(val);
  });
}

function populateTableRow(data)
{
  var description = "<td>" + data[0] + "</td>";
  var gps   = "<td>" + data[1] + " " + data[2] + "</td>";
  $('#markertable').append("<tr>" + description + gps + "</tr>");
}

google.maps.event.addDomListener(window, 'load', initialize);